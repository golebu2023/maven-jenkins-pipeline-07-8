def buildJar(){
    echo "building the application"
    sh "mvn package"
}

def buildImage(){
    echo "building the docker image"
    withCredentials([usernamePassword(credentialsId:"dockerhub-credentials", usernameVariable: 'USR', passwordVariable: 'PWD')]){
        sh 'docker build --tag golebu2023/image-registry:jma2.0 .'
        sh "echo $PWD | docker login -u $USR --password-stdin"
        sh 'docker push golebu2023/image-registry:jma2.0'
    }
}

def deployApp(){
    echo "deploying the application..."
}

return this